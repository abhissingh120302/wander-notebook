import React from "react";

const Alert = ({ type, show, message, onDismiss }) => {

    const margintop = show ? "200px" : "0px";
    const display = show ? "1" : "0";

    return <div className="note-alert-wrapper" style={{ position: "fixed", display: show ? "" : "none", top: 0, bottom: 0, left: 0, right: 0, zIndex: "1000" }}>
        {/* <div className="note-alert-background" style={{ position: "fixed", top: 0, bottom: 0, left: 0, right: 0, zIndex: "1000" }}>
    </div> */}
        <div className="note-alert-message" style={{ width: "350px", opacity: display, margin: "200px auto", marginTop: margintop, borderRadius: "6px", padding: "15px 30px", backgroundColor: type === "error" ? "#d55c4b" : "#67b95b", color: "white", transition: "margin-top 0.15s ease-in-out" }}>
            {type === "error" ? <h5> Error</h5> : <h5>Success</h5>}
            <p style={{ padding: "10px 0" }}>{message}</p>
            <input type="button" value="Ok" onClick={onDismiss} style={{ width: "100px", border: "none", display: "block", margin: "0 auto", padding: "5px 10px", textAlign: "center", backgroundColor: type === "error" ? "#d04b38" : "#195a28", color: "white" }} />
        </div>
    </div>;
};

export const SuccessAlert = (props) => <Alert type="success" {...props} />;
export const ErrorAlert = (props) => <Alert type="error" {...props} />;
