import React from "react";
var editIcon = require('../icons/svg/si-glyph-document-edit.svg');
var deleteIcon = require('../icons/svg/si-glyph-delete.svg');
var viewIcon = require('../icons/svg/si-glyph-view.svg');

const styles = {
  icon: {
    height: 20,
    width: 20
  }
};


const getFooterMessage = (creationTime) => {
  creationTime = new Date(creationTime)
  var now = new Date();
  console.log(now);
  console.log(creationTime);

  var elapsedTime = (now - creationTime) / 1000;
  elapsedTime = Math.floor(elapsedTime);
  console.log(elapsedTime);
  var message = '';
  if (elapsedTime < 60) {
    message = `Created ${elapsedTime} seconds ago`;
  }
  else if (elapsedTime > 60 && elapsedTime < 3600) {
    message = `Created ${Math.round(elapsedTime / 60)} minutes ago`;
  }
  else if (elapsedTime > 3600 && elapsedTime < 86400) {
    message = `Created ${Math.round(elapsedTime / 60 / 60)} hours ago`;
  } else {
    message = `Created ${Math.round(elapsedTime / 60 / 60 / 24)} days ago`;
  }
  return message;

}



export default ({
  title,
  description,
  onView,
  onEdit,
  onSelect,
  onDelete,
  selected,
  creationTime,
  updateTime,
  id
}) => {
  return (
    <div className="col-sm-3 col-sm-offset-0">
      <div className="card border-dark mb-3" >
        <div className="card-header">
          {title}
          <div className="btn-group float-right" role="group">

            <button type="button" className="btn btn-sm"
              onClick={() => {
                onEdit(id);
              }}>

              <img src={editIcon} style={styles.icon} />

            </button>


            <button type="button" className="btn  btn-sm float-right"
              onClick={() => {
                onDelete(id);
              }}>

              <img src={deleteIcon} style={styles.icon} />
            </button>

            {/* <button
                type="button"
                className="btn btn-sm"
                data-toggle="modal"
                data-target="#myModal"
                onClick={() => {
                  onView({ title, description });
                }}
              >
                
               <img src={viewIcon} style={styles.icon}/>
              </button> */}
          </div></div>
        <div className="card-body text-dark">
          {/* <h5 className="card-title"></h5> */}
          <p className="card-text">{description}</p>
        </div>
        <div className="card-footer text-muted">
          {
            getFooterMessage(creationTime)

          }          </div>
      </div>
    </div>


    // <div
    //   className="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0"
    //   style={{ overflow: "auto" }}
    // >
    //   <div className="note" style={styles.note}>
    //     <h3 className="note-head" style={styles.noteHead}>
    //       <span
    //         className={`glyphicon ${
    //           !selected ? "glyphicon-unchecked" : "glyphicon-check"
    //         }`}
    //         style={{ fontSize: "16px" }} onClick={() => {onSelect(id)}}
    //       />{" "}
    //       {title}
    //     </h3>
    //     <span>CREATED: {creationTime}</span>
    //     <br />
    //     <span>UPDATED: {updateTime}</span>
    //     <p style={styles.noteDescription}>{description}</p>
    //     <div style={styles.separator} />
    //     <div style={styles.actions}>
    //       <div
    //         className="btn-group btn-group-justified"
    //         role="group"
    //         aria-label="..."
    //       >
    //         <div className="btn-group" role="group">
    //           <button type="button" class="btn btn-primary"
    //            onClick={() => {
    //             onEdit(id);
    //           }}>
    //             <span
    //               className="glyphicon glyphicon-pencil"

    //             />
    //           </button>
    //         </div>
    //         <div className="btn-group" role="group">
    //           <button type="button" className="btn btn-danger"
    //           onClick={() => {
    //             onDelete(id);
    //           }}>
    //             <span
    //               className="glyphicon glyphicon-trash"
    //               // onClick={() => {
    //               //   onDelete(id);
    //               // }}
    //             />
    //           </button>
    //         </div>
    //         <div className="btn-group" role="group">
    //           <button
    //             type="button"
    //             class="btn btn-success"
    //             data-toggle="modal"
    //             data-target="#myModal"
    //             onClick={() => {
    //               onView({ title, description });
    //             }}
    //           >
    //             <span className="glyphicon glyphicon-eye-open" />
    //           </button>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // </div>
  );
};

