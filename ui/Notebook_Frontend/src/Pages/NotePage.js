import React from "react";
//import Note from "../components/Note";
import NewNote from '../components/NewNote';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as api from "../api";
import { ErrorAlert, SuccessAlert } from "../components/Alert";

const NavigationBar = ({ username, onLogout, onNoteAdd }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <a className="navbar-brand" href="#">NoteBook</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavDropdown">
        <ul className="navbar-nav  mr-auto">
          <li className="nav-item active">
            <button
              type="button"
              className="btn btn-info"
              onClick={() => { onNoteAdd(); console.log('hello'); }}
            >
              <span style={{ marginLeft: "6px" }}>Add Note</span>
            </button>
            {/* <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> */}
          </li>

        </ul>
        {/* <div className="float-right"> */}
        <span
          className="navbar-text navbar-right"
          style={{ marginRight: "10px" }}>
          Signed in as {username} | <span className="navbar-link" onClick={onLogout}>logout</span>
        </span>
        {/* </div> */}

      </div>
    </nav>

    // <nav
    //   className="navbar navbar-dark bg-dark"
    //   style={{ height: "80px" }}
    // >
    //   <div className="container-fluid">
    //     <div className="navbar-header">
    //       <span className="navbar-brand">
    //         <span
    //           className="glyphicon glyphicon-grain"
    //           style={{ fontSize: "30px", color: "white" }}
    //         />
    //       </span>
    //       <span style={{ fontSize: "20px", color: "white", display: "inline-block", paddingTop: "20px"}}>Notebook</span>
    //     </div>
    //     {/** <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-5"> */}
    //     <span
    //       className="navbar-text navbar-right"
    //       style={{ marginRight: "10px" }}>
    //       Signed in as {username} | <span className="navbar-link" onClick={onLogout}>logout</span>
    //     </span>
    //   </div>
    // </nav>
  );
};

const SideBar = ({ onNoteAdd, onNoteDelete }) => {
  return (
    <div className="btn-group">
      <button
        type="button"
        className="btn btn-info"
        onClick={onNoteAdd}
      >
        <span style={{ marginLeft: "6px" }}>Add Note</span>
      </button>
      <br />
      <br />

      <button
        type="button"
        className="btn btn-warning"
        onClick={onNoteDelete}
      >
        Delete Note
      </button>
    </div>

  );
};

const NotePage = ({
  username,
  addNote,
  notes: Notes,
  updateNote,
  deleteNote
}) => {
  const [pageNew, setPageNew] = React.useState(false);
  const [pageEdit, setPageEdit] = React.useState(false);
  const [pageHome, setPageHome] = React.useState(false);
  const [noteId, setNoteId] = React.useState(null);
  const [selectedNotes, setSelectedNotes] = React.useState([]);
  const [ alert, setAlert ] = React.useState({ show: false, message: "" });
  const [ alertError, setAlertError] = React.useState(false);

  React.useEffect(() => {

    let search = window.location.search;
    let params = new URLSearchParams(search);
    let operation = params.get('operation');

    if(operation === "created"){
      setAlert({ show: true, message: "Note created successfully!!"});
    }
    else if(operation === "updated"){
      setAlert({ show: true, message: "Note updated successfully!!"});
    }

    api.getNotes(username)
      .then(data => {
        addNoteAndRenderToast(data.data).then(() => {
          //  renderToast('Note added Successfully','success')
        });
        // console.log(response);
      })
    //.
    //then(()=>renderToast('Note retrieved Successfully','info'));
  }, []);

  const logout = () => {
    localStorage.clear();
    setPageHome(true);
  };

  const addNoteAndRenderToast = async (data) => {
    console.log(data);
    await addNote(data)
  }
  const handleNoteSelect = id => {
    const note = Notes.filter(data => data.id === id);
    if (note.length > 0) {
      const isSelected = note[0].selected;
      const newSelections = selectedNotes.slice();
      if (isSelected) {
        newSelections.splice(newSelections.indexOf(id), 1);
      } else {
        // eslint-disable-next-line no-unused-expressions
        newSelections.indexOf(id) === -1 ? newSelections.push(id) : null;
      }
      console.log(newSelections);
      setSelectedNotes(newSelections);
      updateNote({ ...note[0], selected: !isSelected });
    }
  };

  const handleNew = async () => {
    await setPageNew(true);
    //console.log('New Note added successfully' );
    // renderToast();
  };

  const renderToast = (message, classval) => {
    console.log('rendering message');
    document.getElementById('message').innerHTML = `
<div class="alert alert-${classval} alert-dismissible fade show" role="alert">
  <strong> ${message}!</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>`;
  }

  const getNotes = () => {
    const NotesIn4 = [];
    let startingIndex = 0;
    for (let i = 0; i < Notes.length; i++) {
      if ((i + 1) / 4 === 0) {
        startingIndex++;
      }
      if (!NotesIn4[startingIndex]) {
        NotesIn4[startingIndex] = [];
      }
      NotesIn4[startingIndex].push(Notes[i]);
    }
    return NotesIn4;
  };

  const handleDelete = id => {
    if (typeof id === "number") {
      api.deleteNote(id).then(() => {
        deleteNote(id);
      }).then(() => {
        setAlert({ show: true, message: "Deleted successfully"});
        // renderToast('Deleted Successfully', 'danger');
      });
    } else {
      const allPromise = [];
      const allIds = selectedNotes.slice();
      allIds.forEach(d => {
        allPromise.push(api.deleteNote(d));
      });

      Promise.all(allPromise).then(() => {
        setAlert({ show: true, message: "Deleted successfully"});
        allIds.forEach(d => deleteNote(d));
        setSelectedNotes([]);
      });
    }
  };

  const handleEdit = id => {
    setPageEdit(true);
    setNoteId(id);
  };

  const [note, setNote] = React.useState({
    title: "",
    description: "",
    creationTime: "",
    updateTime: ""
  });
  if (pageHome) {
    return (
      <Redirect
        to={{
          pathname: "/",
          state: { id: noteId }
        }}
      />
    );
  } else if (pageEdit) {
    return (
      <Redirect
        to={{
          pathname: "/notes/edit",
          state: { id: noteId }
        }}
      />
    );
  } else if (pageNew) {
    // renderToast('Note added Successfully','success')
    return <Redirect to="/notes/create" />;
  } else {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let operation = params.get('operation');
    //

    return (
      <React.Fragment>
        <NavigationBar onLogout={logout} username={username} onNoteAdd={handleNew} />
        <SuccessAlert message={alert.message} show={alert.show} onDismiss={() => {
          setAlert(false);
        }}/>
        <div className="container-fluid" >
          <div className="row">
            <div class='col-sm-12' id="message">
              {/* {operation === "created" ?
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong> Note Added Successfully !</strong>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div> : undefined
              } */}
              {/* {operation === "updated" ?
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                  <strong> Note updated Successfully !</strong>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div> : undefined
              } */}
            </div>
          </div>

          <br />

          <div className="row">
            <div className="col-sm-12 col-sm-offset-1">
              {getNotes().map((notesArray, i1) => {
                return (
                  <div className="row" key={i1}>
                    {notesArray.map((data, key) => (
                      <NewNote
                        key={key}
                        {...data}
                        onSelect={handleNoteSelect}
                        onDelete={handleDelete}
                        onEdit={handleEdit}
                        onView={note => setNote(note)}
                      />
                    ))}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        {/* </div> */}
        <div
          className="modal fade"
          id="myModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="myModalLabel"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 className="modal-title" id="myModalLabel">
                  {note.title}
                </h4>
              </div>
              <div className="modal-body">{note.description}</div>
            </div>
          </div>

        </div>
      </React.Fragment>
    );
  }
};

export default connect(
  state => ({ notes: state.notes, username: state.username }),
  dispatch => ({
    deleteNote: (...ids) => {
      ids.forEach(id => {
        dispatch({ type: "DELETE_NOTE", payload: id });
      });
    },
    updateNote: data => {
      dispatch({ type: "UPDATE_NOTE", payload: data });
    },
    addNote: data => {
      dispatch({ type: "INITIAL_NOTE", payload: data });
    }
  })
)(NotePage);

