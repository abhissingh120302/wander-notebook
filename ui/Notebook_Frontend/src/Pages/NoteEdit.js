import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as api from "../api";
import { ErrorAlert } from "../components/Alert";

const EditNote = ({ location, updateNote, notes }) => {
    const [homePage, setHomePage] = React.useState({ redirect: false, operation: "" });
    const [alert, setAlert] = React.useState({ show: false, message: ""});
    const [noteId, setNoteId] = React.useState(null);
    const noteTitle = React.useRef(null);
    const noteDescription = React.useRef(null);

    React.useEffect(() => {    
        if(location.state && location.state.id){
            setNoteId(location.state.id);

            const note = notes.filter((data) => {            
                return data.id === location.state.id;
            });
            if(noteTitle.current !== null && noteDescription.current !== null){
                noteTitle.current.value = note[0].title;
                noteDescription.current.value = note[0].description;
            }   
        }
        else{
            setHomePage({operation: "", redirect: true});
        }
    });

    const handleUpdate = () => {
        api.editNote(noteId, noteDescription.current.value, noteTitle.current.value)
            .then(
                () => {
                    setHomePage({ redirect: true, operation: "updated" });
                },
                error => {
                    console.log(error);
                    setAlert({show: true, message: JSON.stringify(error.response.data) });
                }
            )
        // updateNote(noteId, noteTitle.current.value, noteDescription.current.value);
    };

    if (homePage.redirect) {
        return <Redirect to={`/notes?operation=${homePage.operation}`} />;
    }
    else {
        return <React.Fragment>
            <ErrorAlert show={alert.show} message={`Error updating a note\n${alert.message}`} onDismiss={() => setAlert({show: false, message: ""})} />
            <div className="container">
                <div className="row">
                    <div className="col-xs-10 col-sm-6 col-sm-offset-3">
                        <h3>Edit a Note</h3>
                        <hr />
                        <div className="form-group">
                            <label htmlFor="txtTitle">Title</label>
                            <input type="text" readOnly={true} className="form-control" id="txtTitle" placeholder="Enter the title of the note" ref={noteTitle} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="txtDescription">Description</label>
                            <textarea className="form-control" id="txtDescription" rows="3" ref={noteDescription}></textarea>
                        </div>
                        <button type="button" className="btn btn-default" onClick={handleUpdate}>Update</button>
                        <button type="button" className="btn btn-default" onClick={() => {
                            setHomePage({ redirect: true, operation: "" })
                        }}>Cancel</button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    }
}

export default connect(
    (state) => ({
        notes: state.notes
    }),
    (dispatch) => ({
        updateNote: (noteId, noteTitle, noteDescription) => {
            dispatch({
                type: "UPDATE_NOTE", payload: {
                    id: noteId,
                    title: noteTitle,
                    description: noteDescription
                }
            });
        }
    })
)(EditNote);