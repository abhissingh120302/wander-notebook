import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as api from "../api";
import { ErrorAlert } from "../components/Alert";

const CreateNote = ({ addNote, username }) => {
    const [homePage, setHomePage] = React.useState({redirect: false, operation: ""});
    const [alert, setAlert] = React.useState({show: false, message: ""});
    const noteTitle = React.useRef(null);
    const noteDescription = React.useRef(null);

    const handleNoteCreate = () => {
        api.addNote(username, noteTitle.current.value, noteDescription.current.value)
            .then(
                () => {
                    // addNote(noteTitle.current.value, noteDescription.current.value);
                      setHomePage({redirect: true, operation: "created"});
                },
                (error) => {
                    console.log(error.response.data);
                    setAlert({ show: true, message: JSON.stringify(error.response.data) });
                }
            )
        //   setHomePage(true);
    };

    const handleCancel = () => {
        noteTitle.current.value = "";
        noteDescription.current.value = "";
          setHomePage({redirect: true, operation: ""});
    };

    if (homePage.redirect) {
        return <Redirect to={`/notes?operation=${homePage.operation}`} />;
    }
    else {
        return <React.Fragment>
            <ErrorAlert show={alert.show} message={`Error creating a note\n${alert.message}`} onDismiss={() => setAlert({ show: false, message: "" })}/>
            <div className="container">
                <div className="row">
                    <div className="col-xs-10 col-sm-6 col-sm-offset-3">
                        <h3>Create a Note</h3>
                        <hr />
                        <div className="form-group">
                            <label htmlFor="txtTitle">Title</label>
                            <input type="text" ref={noteTitle} className="form-control" id="txtTitle" placeholder="Enter the title of the note" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="txtDescription">Description</label>
                            <textarea className="form-control" ref={noteDescription} id="txtDescription" rows="3"></textarea>
                        </div>
                        <button type="button" className="btn btn-default" onClick={handleNoteCreate}>Create</button>
                        <button type="button" className="btn btn-default" onClick={handleCancel}>Cancel</button>
                    </div>
                </div>
            </div>
        </React.Fragment>;
    }
};

export default connect(
    ({ username }) => ({ username }),
    (dispatch) => ({
        addNote: (title, description) => {
            dispatch({ type: "ADD_NOTE", payload: { id: 4, title, description } });
        }
    })
)(CreateNote);
